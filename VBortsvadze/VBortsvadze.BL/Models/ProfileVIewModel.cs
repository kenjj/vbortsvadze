﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Models
{
    public class ProfileViewModel
    {
        public List<VInfo> Info { get; set; }
        public string InfluenceImage { get; set; }
        public string KloutNumber { get; set; }
        public string TweeterImage { get; set; }
    }
}