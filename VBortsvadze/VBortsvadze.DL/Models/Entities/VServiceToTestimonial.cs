﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class VServiceToTestimonial
    {
        public int Id { get; set; }
        [ForeignKey("VService")]
        public int VServiceId { get; set; }
        public VService VService { get; set; }
        [ForeignKey("Testimonial")]
        public int TestimonialId { get; set; }
        public Testimonial Testimonial { get; set; }
    }
}