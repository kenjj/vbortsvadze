﻿$(document).ready(function(){
               
    var controllerName = "testimonials";

    $("#table").bootgrid({
        css: myBootgrid.style,
        formatters: {
            "commands": function (column, row) {
                return myBootgrid.getActionLink("edit", row.id, controllerName) + myBootgrid.getActionLink("delete", row.id, controllerName);
            }
        }
    });

});
