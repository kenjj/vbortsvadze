﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace VBortsvadze.BL.Utils
{
    public static class CommonUtils
    {



        public static Image Base64ToImage(this string base64String)
        {
            try
            {
                base64String = base64String.Substring(base64String.IndexOf(',') + 1);
                byte[] imageBytes = Convert.FromBase64String(base64String);
                MemoryStream ms = new MemoryStream(imageBytes, 0,
                  imageBytes.Length);

                // Convert byte[] to Image
                ms.Write(imageBytes, 0, imageBytes.Length);
                Image image = Image.FromStream(ms, true);
                return image;
            }
            catch (Exception)
            {
                return null;
            }
           
        }

        public static void DeleteFile(string Path)
        {
            try
            {
                FileInfo file = new FileInfo(Path);
                if (file.Exists)
                {
                    file.Delete();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        public static bool SaveImage(this Image image, string folderPath, string filename)
        {
            try
            {
                string filePathImage = getPath(folderPath,filename);
                image.Save(filePathImage, System.Drawing.Imaging.ImageFormat.Png);
                image.Dispose();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static bool SaveFile(this HttpPostedFileBase File,string folderPath, string filename)
        {
            try
            {
               
                if (!File.isImageFileValid())
                    throw new Exception("Not Valid File");
                
                var fileExtension = File.getFileExtension();
                filename += fileExtension;

                var path = getPath(folderPath, filename);

                File.SaveAs(path);

                return true;

            }

            catch (Exception)
            {
                return false;
            }
        }

        public static bool isImageFileValid(this HttpPostedFileBase File)
        {
            try
            {
                if (File == null)
                    throw new Exception("Not Passed File");

                var fileExtension = File.getFileExtension();

                if (fileExtension == ".jpg" ||
                    fileExtension == ".png" ||
                    fileExtension == ".gif" ||
                    fileExtension == ".jpeg")
                {
                    var size = File.ContentLength;

                    if (size > CommonValues.MaxImageSize)
                        throw new Exception("Not Allowed Size");

                    return true;
                }
                else
                    throw new Exception("Not Valid Extension");
                
            }
            catch (Exception)
            {
                return false;
            }
            

        }

        public static string getFileExtension(this HttpPostedFileBase File)
        {
            return Path.GetExtension(File.FileName).ToLower();
        }

        public static string getPath(string folderPath, string filename)
        {
            return Path.Combine(HostingEnvironment.MapPath(folderPath), filename);
        }
       


    }
}