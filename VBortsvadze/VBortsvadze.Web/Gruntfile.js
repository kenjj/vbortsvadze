module.exports = function (grunt) {
    // Project configuration.

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: "\n"
            },
            dist: {
                src: ['Assets/js/lib/*.js', 'Assets/js/utils/script.js'],
                dest: 'Assets/js/script.js'
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'compressed'

                },
                files: {                                    // Dictionary of files
                    'Assets/css/style.preprocessed.css': 'Assets/css/style.scss'         // 'destination': 'source'
                }
            }
        },
        postcss: {
            options: {
                map: {
                    inline: false
                },
                processors: [
                  require('autoprefixer')({ browsers: '> 0.5%' }) // add vendor prefixes
                ]
            },
            dist: {
                src: 'Assets/css/style.preprocessed.css',
                dest: 'Assets/css/style.css'
            }
        },
        uglify: {
            options: {
                sourceMap: true,
                mangle: false
            },
            my_target: {
                files: {
                    'Assets/js/script.min.js': ['Assets/js/script.js']
                }
            }
        },
        watch: {
            css: {
                files: ['Assets/css/*.scss', 'Assets/css/utils/*.scss'],
                tasks: ['sass', 'postcss']
            },
            scripts: {
                files: ['Assets/js/lib/*.js', 'Assets/js/utils/*.js'],
                tasks: ['concat']
                
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');

    // Default task(s).
    grunt.registerTask('default', ['watch']);

};