﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Services
{
    public class PublicationService
    {
        private EFDbContext db;
        public PublicationService(EFDbContext _db)
        {
            db = _db;
        }

        public List<Publication> getAll()
        {
            return db.Publications.ToList();
        }

    }
}