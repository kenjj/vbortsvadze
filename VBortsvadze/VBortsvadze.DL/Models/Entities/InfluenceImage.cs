﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class InfluenceImage
    {
        [Key]
        public int Id { get; set; }
        public string Image { get; set; }
    }
}