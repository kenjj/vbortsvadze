﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class BlogToCategory
    {
        public int Id { get; set; }

        [ForeignKey("Blog")]
        public int BlogId { get; set; }
        public Blog Blog { get; set; }

        [ForeignKey("BlogCategory")]
        public int CategoryId { get; set; }
        public BlogCategory BlogCategory { get; set; }

    }
}