﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class Statistic
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MaximumValue { get; set; }
        public int CurrentValue { get; set; }
        public string DataType { get; set; }
    }
}