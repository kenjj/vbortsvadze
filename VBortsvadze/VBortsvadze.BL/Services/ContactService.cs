﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.BL.Models;
using VBortsvadze.BL.Utils;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Services
{
    public class ContactService
    {
        private EFDbContext db;
        public ContactService(EFDbContext _db)
        {
            db = _db;
        }

        public bool SendEmail(ContactMailModel item)
        {

            var message =
            "<div>" +
                "<h3>Message From " + item.Name + "</h3>" +
                "<h4> subject - " + item.Subject + "</h4>" +
                "<p>" + item.Content + "</p>" +
                "<p></p>" +
                "<h4> Contact Information - " + item.Email +
            "</div>";

            try
            {
                EmaliHelper.SendEmail(message);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public void addEmail(string email) {

            if (db.Subscribers.Any(q => q.Email == email))
                throw new Exception("Email Already Registered");
            var mail = new Subscriber
            {
                Email = email
            };
            db.Subscribers.Add(mail);
            db.SaveChanges();

        }


    }
}