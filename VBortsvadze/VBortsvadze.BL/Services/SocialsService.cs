﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace VBortsvadze.BL.Services
{
    public class SocialsService
    {
       
        private string oAuthUrl = "https://api.twitter.com/oauth2/token";
        private string screenname = "VladoBotsvadze";
        private string oAuthConsumerSecret = "PpSFN2DSSm1GjuKnpgqMSmbP2yQcqNdixYcZ17nPxhFRssIL5x";
        private string oAuthConsumerKey = "PX7VokvaU8efEM8bvBhagF15T";
        private TwitAuthenticateResponse twitAuthResponse { get; set; }
        public SocialsService()
        {
            
        }



        public async System.Threading.Tasks.Task<string> getFBTimeLine()
        {
            var app_id = "1848875225390706";
            var app_secret = "1a5317c7545417cdab5f17803474a381";
            var token = "";

            using (var client = new HttpClient())
            {
                var url = "https://graph.facebook.com/oauth/access_token?grant_type=client_credentials&client_id="+ app_id + "&client_secret=" + app_secret + "";
                var responseString = await client.GetStringAsync(url);
                token = responseString;
            }

            using (var client = new HttpClient())
            {
             
                var url = "https://graph.facebook.com/tbcbank/feed?" + token;
                var responseString = await client.GetStringAsync(url);
                return responseString;

            }
        }
        public string getTweeterTimeLine()
        {
            // You need to set your own keys and screen name


            // Do the Authenticate
            var authHeaderFormat = "Basic {0}";

            var authHeader = string.Format(authHeaderFormat,
                Convert.ToBase64String(Encoding.UTF8.GetBytes(Uri.EscapeDataString(oAuthConsumerKey) + ":" +
                Uri.EscapeDataString((oAuthConsumerSecret)))
            ));

            var postBody = "grant_type=client_credentials";

            HttpWebRequest authRequest = (HttpWebRequest)WebRequest.Create(oAuthUrl);
            authRequest.Headers.Add("Authorization", authHeader);
            authRequest.Method = "POST";
            authRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
            authRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (Stream stream = authRequest.GetRequestStream())
            {
                byte[] content = ASCIIEncoding.ASCII.GetBytes(postBody);
                stream.Write(content, 0, content.Length);
            }

            authRequest.Headers.Add("Accept-Encoding", "gzip");

            WebResponse authResponse = authRequest.GetResponse();
            // deserialize into an object

            using (authResponse)
            {
                using (var reader = new StreamReader(authResponse.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objectText = reader.ReadToEnd();
                    twitAuthResponse = JsonConvert.DeserializeObject<TwitAuthenticateResponse>(objectText);
                }
            }


            var timelineFormat = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name={0}&include_rts=1&exclude_replies=1&count=5";
            var timelineUrl = string.Format(timelineFormat, screenname);
            HttpWebRequest timeLineRequest = (HttpWebRequest)WebRequest.Create(timelineUrl);
            var timelineHeaderFormat = "{0} {1}";
            timeLineRequest.Headers.Add("Authorization", string.Format(timelineHeaderFormat, twitAuthResponse.token_type, twitAuthResponse.access_token));
            timeLineRequest.Method = "Get";
            WebResponse timeLineResponse = timeLineRequest.GetResponse();
            var timeLineJson = string.Empty;
            using (timeLineResponse)
            {
                using (var reader = new StreamReader(timeLineResponse.GetResponseStream()))
                {
                    timeLineJson = reader.ReadToEnd();
                }
            }
            return timeLineJson;
        }
       
    }
    public class TwitAuthenticateResponse
    {
        public string token_type { get; set; }
        public string access_token { get; set; }
    }
}