﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.BL.Models;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Services
{
    public class RecognitionService
    {
        private EFDbContext db;
        public RecognitionService(EFDbContext _db)
        {
            db = _db;
        }

        public List<RecognitionViewModel> getAll()
        {
            var model = db.Recognitions.GroupBy(q => q.Date.Year).Select(o => new RecognitionViewModel
            {
                Year = o.Key,
                List = o.OrderBy(q => q.OrderIndex).ToList()         
            }).OrderByDescending(q=>q.Year).ToList();
            return model;
        }

        

        public List<Award> getOne()
        {
            var model = db.Awards.OrderBy(q => q.OrderIndex).ToList();
            return model;
        }
    }
}