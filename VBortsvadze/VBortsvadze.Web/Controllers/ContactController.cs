﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using System.Web.Mvc;
using VBortsvadze.BL.Models;
using VBortsvadze.BL.Services;
using VBortsvadze.DL.Models;

namespace VBortsvadze.Web.Controllers
{
    public class ContactController : ControllerBase
    {
        EFDbContext db = new EFDbContext();
        // GET: Contact
        public ActionResult Index()
        {
            ViewBag.Title = "Contact";
            ViewBag.testimonials = Service.Testimonials.getByPage("contact");
            return View();
        }


        [HttpPost]
        public ActionResult Index(ContactMailModel message)
        {
            ViewBag.Title = "Contact";
            var response = "Message Not Send, Try Again";

            if (ModelState.IsValid)
            {
                
                var messageSend = Service.Contact.SendEmail(message);
                if (messageSend)
                {
                    response = "Message Sent, Thank You";
                }

            }

            ViewBag.response = response;








            ViewBag.testimonials = Service.Testimonials.getByPage("contact");
            return View();
        }


    }
}