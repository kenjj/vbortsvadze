﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.ViewModels.Admin.Account;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult Login()
        {
            if (Session[BL.Utils.CommonValues.AdminSessionKey] != null)
            {
                return RedirectToAction("index", "Home");
            }
            var model = new LoginViewModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (model.Username == "vladimer" && model.Password == "vladimer,055")
                    {
                        Session[BL.Utils.CommonValues.AdminSessionKey] = true;
                        return RedirectToAction("index", "Home");
                    }
                }
            }
            catch (Exception)
            {

            }

            ModelState.AddModelError("ErrorMessage", "Wrond Username/Password");
            return View(model);
        }

        public ActionResult Logout()
        {
            Session[BL.Utils.CommonValues.AdminSessionKey] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}