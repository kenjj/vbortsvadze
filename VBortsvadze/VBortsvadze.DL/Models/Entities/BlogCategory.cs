﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class BlogCategory
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public int OrderIndex { get; set; }

        public List<BlogToCategory> Blogs { get; set; }
    }
}