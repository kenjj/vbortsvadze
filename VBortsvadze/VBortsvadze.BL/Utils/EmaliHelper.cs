﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace VBortsvadze.BL.Utils
{
    public static class EmaliHelper
    {
        public static void SetConfig(NameValueCollection appSettings)
        {
            AppSettings = appSettings;
            Init();
        }

        private static void Init()
        {
            _SmtpServer = AppSettings["SmtpServer"];
            _SmtpPort = Convert.ToInt32(AppSettings["SmtpPort"]);
            _LoginEmail = AppSettings["LoginEmail"];
            _LoginPassword = AppSettings["LoginPassword"];
            _EnableSSL = Convert.ToBoolean(AppSettings["EnableSSL"]);


            _BccAll = AppSettings["BccAll"];
            _EmailFrom = AppSettings["EmailFrom"];
            _EmailTo = AppSettings["EmailTo"];
        }

        public static NameValueCollection AppSettings;

        private static string _SmtpServer;
        private static int _SmtpPort;
        private static string _LoginEmail;
        private static string _LoginPassword;
        private static bool _EnableSSL;

        private static string _BccAll;
        private static string _EmailFrom;
        private static string _EmailTo;


        public static void SendEmail(string body)
        {
            var from = _EmailFrom;
            var to = _EmailTo;
            var subject = "Message From Vladimerbotsvadze.com";


            var message = new MailMessage(from, to, subject, body) { IsBodyHtml = true };
            SendEmail(to,message);


        }

        public static void SendEmailByAddress(string email,string body)
        {
            var from = _EmailFrom;
            var to = email;
            var subject = "Message From Vladimerbotsvadze.com";


            var message = new MailMessage(from, to, subject, body) { IsBodyHtml = true };
            SendEmail(to,message);


        }


        public static void SendEmail(string subject, string body, string from, string to)
        {
            var message = new MailMessage(from, to, subject, body) { IsBodyHtml = true };
            SendEmail(to,message);
        }



        public static void SendEmail(string mail, MailMessage message)
        {
            var body = message.Body;
            message.Body = body;

            var objSmtpClient = new SmtpClient(_SmtpServer, _SmtpPort);

            if (!String.IsNullOrEmpty(_BccAll))
                message.Bcc.Add(_BccAll);

            if (!String.IsNullOrEmpty(_EmailFrom))
                message.From = new MailAddress(_EmailFrom);

            if (!String.IsNullOrEmpty(mail))
            {
                message.To.Clear();
                message.To.Add(new MailAddress(mail));
            }

            objSmtpClient.EnableSsl = _EnableSSL;
            objSmtpClient.UseDefaultCredentials = false;

            objSmtpClient.Credentials = new NetworkCredential(_LoginEmail, _LoginPassword);
            objSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;

            // If this segment will be Uncommented, nonsecured Error will be handled in ex
            try
            {
                objSmtpClient.Send(message);
            }
            catch (Exception ex)
            {
                var exe = ex;
            }

        }
    }
}