﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models;

namespace VBortsvadze.BL.Services._Common
{
    public class AppService : ServiceBase, IDisposable
    {
        public AppService(EFDbContext db)
           : base(db)
        {
            Blogs = new BlogService(db);
            Publication = new PublicationService(db);
            Recognition = new RecognitionService(db);
            Testimonials = new TestimonialService(db);
            Statistic = new StatisticService(db);
            Contact = new ContactService(db);
        }

        public ContactService Contact { get; set; }

        public BlogService Blogs { get; set; }
        public PublicationService Publication { get; set; }
        public RecognitionService Recognition { get; set; }
        
        public TestimonialService Testimonials { get; set; }

        public StatisticService Statistic { get; set; }


        public void Dispose()
        {
            Db.Dispose();
        }
    }
}