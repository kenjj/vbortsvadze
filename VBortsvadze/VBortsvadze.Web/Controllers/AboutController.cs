﻿using System;
using System.Collections.Generic;
using System.Linq;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Models;

namespace VBortsvadze.Web.Controllers
{
    public class AboutController : ControllerBase
    {
        // GET: About
        public ActionResult Index()
        {
     
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            ViewBag.Recognitions = Service.Recognition.getOne();
            ViewBag.testimonials = Service.Testimonials.getByPage("home");
            ViewBag.Statistic = Service.Statistic.getHomeStatistic();


            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.Title = "About Vladimer";


            return View();
        }
    }
}