﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class SocMediaNumbersController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/SocMediaNumbers
        public ActionResult Index()
        {
            return View(db.SocMediaNumbers.ToList());
        }

        // GET: Admin/SocMediaNumbers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SocMediaNumber socMediaNumber = db.SocMediaNumbers.Find(id);
            if (socMediaNumber == null)
            {
                return HttpNotFound();
            }
            return View(socMediaNumber);
        }

        // GET: Admin/SocMediaNumbers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/SocMediaNumbers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ActivityCount")] SocMediaNumber socMediaNumber)
        {
            if (ModelState.IsValid)
            {
                db.SocMediaNumbers.Add(socMediaNumber);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(socMediaNumber);
        }

        // GET: Admin/SocMediaNumbers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SocMediaNumber socMediaNumber = db.SocMediaNumbers.Find(id);
            if (socMediaNumber == null)
            {
                return HttpNotFound();
            }
            return View(socMediaNumber);
        }

        // POST: Admin/SocMediaNumbers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ActivityCount")] SocMediaNumber socMediaNumber)
        {
            if (ModelState.IsValid)
            {
                db.Entry(socMediaNumber).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(socMediaNumber);
        }

        // GET: Admin/SocMediaNumbers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SocMediaNumber socMediaNumber = db.SocMediaNumbers.Find(id);
            if (socMediaNumber == null)
            {
                return HttpNotFound();
            }
            return View(socMediaNumber);
        }

        // POST: Admin/SocMediaNumbers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SocMediaNumber socMediaNumber = db.SocMediaNumbers.Find(id);
            db.SocMediaNumbers.Remove(socMediaNumber);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
