﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.DL.Models
{
    public class EFDbContext : DbContext
    {


        public EFDbContext() : base("DefaultConnection")
        {

        }

        public DbSet<Publication> Publications { get; set; }
        public DbSet<Testimonial> Testimonials { get; set; }
        public DbSet<Award> Awards { get; set; }
        
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<BlogCategory> BlogCategories { get; set; }
        public DbSet<BlogToCategory> BlogToCategorys { get; set; }
        public DbSet<Page> Pages { get; set; }

        public DbSet<PageToTestimonials> PageToTestimonials { get; set; }
        public DbSet<SocialNetworkActivity> SocialNetworkActivities { get; set; }
        public DbSet<SocMediaNumber> SocMediaNumbers { get; set; }
        
        public DbSet<VInfo> VInfos { get; set; }
        public DbSet<KloutNumber> KloutNumber { get; set; }
        public DbSet<InfluenceImage> InfluenceImage { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }


        public System.Data.Entity.DbSet<VBortsvadze.DL.Models.Entities.Recognition> Recognitions { get; set; }

        public System.Data.Entity.DbSet<VBortsvadze.DL.Models.Entities.Statistic> Statistics { get; set; }
    }
}



