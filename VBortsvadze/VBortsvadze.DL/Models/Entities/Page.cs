﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class Page
    {
        [Key]
        [Required]
        [MaxLength(8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Name { get; set; }
        public List<PageToTestimonials> Testimonials { get; set; }
    }
}