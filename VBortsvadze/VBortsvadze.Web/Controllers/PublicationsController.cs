﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using System.Web.Mvc;

namespace VBortsvadze.Web.Controllers
{
    public class PublicationsController : ControllerBase
    {
        // GET: Publications
        public ActionResult Index()
        {
            ViewBag.Title = "Publications";
            var model = Service.Publication.getAll();
            return View(model);
        }
    }
}