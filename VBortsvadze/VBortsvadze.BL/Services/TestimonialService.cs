﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using System.Data.Entity;

namespace VBortsvadze.BL.Services
{
    public class TestimonialService
    {
        private EFDbContext db;
        public TestimonialService(EFDbContext _db)
        {
            db = _db;
        }
        public List<Testimonial> getByPage(string pageName)
        {
            var testimonials = db.PageToTestimonials
                .Include(Queryable=>Queryable.Testimonial)
                .Where(q=>q.PageName==pageName);

            return testimonials.Select(q => q.Testimonial).OrderBy(q=>q.OrderIndex).ToList();
        }
        public List<Testimonial> getAll()
        {
            return db.Testimonials.ToList();
        }
    }
}