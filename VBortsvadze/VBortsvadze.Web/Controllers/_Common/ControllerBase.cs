﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Services._Common;
using VBortsvadze.DL.Models;

namespace VBortsvadze.Web.Controllers._Common
{
    public class ControllerBase : Controller
    {
        protected AppService Service;
        public ControllerBase()
        {
            Service = new AppService(new EFDbContext());
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Service.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}