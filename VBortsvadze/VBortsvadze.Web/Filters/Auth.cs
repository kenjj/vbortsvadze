﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VBortsvadze.Web.Filters
{
    public class Auth : System.Web.Mvc.ActionFilterAttribute, System.Web.Mvc.IActionFilter
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
           
            if (HttpContext.Current.Session[BL.Utils.CommonValues.AdminSessionKey] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                {"Controller", "Maintenance"},
                {"Action", "Index"}
                });
            }
            base.OnActionExecuting(filterContext);
        }
    }
}