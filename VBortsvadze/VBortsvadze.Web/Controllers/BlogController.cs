﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Services._Common;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using VBortsvadze.DL.Models;

namespace VBortsvadze.Web.Controllers
{
    public class BlogController : ControllerBase
    {
        public ActionResult Index(string slag)
        {

            ViewBag.Title = "Blog";

            if (string.IsNullOrEmpty(slag))
            {
                var model = Service.Blogs.GetBlogsAndCategories(15);
                return View(model);
            }
            else
            {
                var model = Service.Blogs.GetCustomBlogBySlag(slag);
                if (model.Blog == null)
                {
                    slag = null;
                    ModelState.Clear();
                    return RedirectToAction("index", new { slag = "" });
                }

                return View("blog", model);
            }
        }

        public ActionResult Category(string id)
        {
            ViewBag.Title = "Blog";
            var categoryName = id;
            if (string.IsNullOrEmpty(categoryName))
            {
                return RedirectToAction("Index");
            }
            else
            {
                var model = Service.Blogs.GetCustomBlogByCategory(categoryName, 15);


                if (model == null)
                    return RedirectToAction("index");

                if (model.Blogs == null)
                    ViewBag.message = "There Are not Blogs in this Category";

                return View("index", model);
            }
        }

        public ActionResult Search(string queryString)
        {
            ViewBag.Title = "Search Results";
            if (string.IsNullOrEmpty(queryString))
                return RedirectToAction("index");
            else
            {
                var model = Service.Blogs.GetCustomBlogBySearch(queryString, 15);
                return View("index", model);
            }

        }
    }
}