﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VBortsvadze.BL.Models;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Services
{
    public class BlogService
    {
        private EFDbContext db;
        public BlogService(EFDbContext _db)
        {
            db = _db;
        }


        public BlogViewModel GetBlogsAndCategories(int count)
        {
            var blogViewModel = new BlogViewModel();


            blogViewModel.Blogs = db.Blogs.OrderBy( b => b.Id).Take(count).ToList();
            blogViewModel.Categories = db.BlogCategories.ToList();
            blogViewModel.CurrentCategoryId = 0;


            return blogViewModel;
        }



        public CustomBlogViewModel GetCustomBlogBySlag(string slag)
        {
            var model = new CustomBlogViewModel();
            model.Blog = db.Blogs.Where( b => b.Slag == slag).Include(s => s.Categories).FirstOrDefault();
            model.Categories = db.BlogCategories.ToList();
            return model;
        }

        public BlogViewModel GetCustomBlogByCategory(string categoryName, int count)
        {
            var category = db.BlogCategories.FirstOrDefault(c => c.CategoryName == categoryName);

            if (category == null)
                return null;

            else
            {
                var blogToCategorys = db.BlogToCategorys.Where(s => s.CategoryId == category.Id);
                var blogs = blogToCategorys.Select(q => q.Blog).OrderBy(b => b.Id).Take(count).ToList();
                

                var blogViewModel = new BlogViewModel();
                blogViewModel.Blogs = blogs;
                blogViewModel.Categories = db.BlogCategories.ToList();
                blogViewModel.CurrentCategoryId = category.Id;

                return blogViewModel;
            }


        }

        public BlogViewModel GetCustomBlogBySearch(string search, int count)
        {
            var result = db.Blogs.Where(b => b.Content.Contains(search) || b.Title.Contains(search)).OrderBy(b => b.Id).Take(count).ToList();

            var blogViewModel = new BlogViewModel();
            blogViewModel.Blogs = result;
            blogViewModel.Categories = db.BlogCategories.ToList();
            blogViewModel.CurrentCategoryId = 0;

            return blogViewModel;
        }
    }
}