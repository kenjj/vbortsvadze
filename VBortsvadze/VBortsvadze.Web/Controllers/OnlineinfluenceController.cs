﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using System.Web.Mvc;
using VBortsvadze.BL.Models;

namespace VBortsvadze.Web.Controllers
{
    public class OnlineInfluenceController : ControllerBase {
        // GET: Onlineinfluence
        public ActionResult Index()
        {
            ViewBag.Title = "Online Influence";
            ViewBag.Recognitions = Service.Recognition.getOne();
            ViewBag.testimonials = Service.Testimonials.getByPage("oninf");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();


            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;

            return View();
        }
    }
}