﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Services;

namespace VBortsvadze.Web.Controllers
{
    public class TPServicesController : Controller
    {
        // GET: TPService
        //public async System.Threading.Tasks.Task<string> Facebook()
        //{
        //    SocialsService facebook = new SocialsService();


        //    string textResult = await facebook.getFBTimeLine();

        //    return textResult;

        //}
        public string Twitter()
        {
        
               
                SocialsService twitter = new SocialsService();


                string textResult = twitter.getTweeterTimeLine();

                return textResult;
            
        }
    }
    public class fbFullData
    {
        public fbFeed feed { get; set; }
    }
    public class fbFeed
    {
        public List<fbPost> data { get; set; }
    }

    public class fbPost
    {
        public string message { get; set; }
        public string permalink_url { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string picture { get; set; }
    }
}