﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Services
{
    public class StatisticService
    {

        private EFDbContext db;
        public StatisticService(EFDbContext _db)
        {
            db = _db;
        }
        public List<SocialNetworkActivity> getNetworkActivity()
        {
            var model = db.SocialNetworkActivities.OrderBy(q => q.Id).ToList();
            return model;
        }
        public List<SocMediaNumber> getSocMediaNumbers()
        {
            var model = db.SocMediaNumbers.OrderBy(q => q.Id).ToList();
            return model;
        }
        public string getInfluenceImage()
        {

            var item = db.InfluenceImage.FirstOrDefault();
            if (item == null) return "";
            else
                return item.Image;
        }
        public string getTweeterImage()
        {

            var item = db.InfluenceImage.FirstOrDefault( a => a.Id == 2);
            if (item == null) return "";
            else
                return item.Image;
        }
        public List<Statistic> getHomeStatistic()
        {
            var model = db.Statistics.OrderBy(q => q.Id).ToList();
            return model;
        }
        public string getCloutNumber()
        {
            var item = db.KloutNumber.FirstOrDefault();
            if (item == null) return "0";
            else
                return item.Number;
        }
        public List<VInfo> getInfos()
        {
            var model = db.VInfos.OrderBy(q => q.Id).ToList();
            return model;
        }
    }
}