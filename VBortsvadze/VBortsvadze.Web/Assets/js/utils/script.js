$(document).ready(function () {


    $("#subscribe-button").on("click", function () {
        $(this).replaceWith("<div class='subscribe'><input name='subscribe' type='email' id='head-subscribe'><div class='subscribe-div'>Subscribe<div></div>");

        $(".vladimer-subscribe-button").on("click", ".subscribe-div", function () {
            var val = $("#head-subscribe").val();
            if (val == "")
                alert("no");
            else {
                var data = {
                    email: val
                }
                $.post(Path + "Home/AddSubscriber", data, function (data) {
                    $(".subscribe").replaceWith("<div class='thank-form-up'>Thank you for subscribing</div>");
                });
            }
        });

    });


    $('.close-bookin-pupup').click(function () {
        $('.bookin-popup-wrapper').fadeOut();
    })


    $('.vl-cons-outer a').click(function (e) {
        e.preventDefault();
        $('.bookin-popup-wrapper').fadeIn();
    })
    //Search Bar Behavior
    $(".search.icon").on("click", function () {
        $(".logo, .nav").css("display", "none");
        $(".shared-search").css("display", "block").find("input[type='text']").focus();

        $(".cancel-button").on("click", function () {
            $(".logo, .nav").css("display", "block");
            $(".shared-search").css("display", "none");
        });
    });


    $('.desc-text span').click(function () {
        var el = $(this).parent('.desc-text');
        el.find("p").toggleClass('less-now');

    });


    $('.burger').click(function () {
        $('.nav').addClass('burger-showed');
        $('.burger').css("opacity", "0");
        $(".shared-search").css("opacity", "1").addClass("active");
    });


    $('.close-burger').click(function () {
        $('.nav').removeClass('burger-showed');
        $('.burger').css("opacity", "1");
        $(".shared-search").css("opacity", "0").removeClass("active");
    });

    $('#subscribe-form').submit(function (e) {
        e.preventDefault();

        var val = $('#email-value-form').val();

        var data = {
            email: val
        }
        $.post(Path + "Home/AddSubscriber", data, function (data) {
            if (data.Error != true) {
                var input = $("#email-value-form").replaceWith("<div class='thank-form'>Thank you for subscribing</div>");
                $("#subscribe-form").find("input").css("display", "none");
            }
        });

    });

    $(".workwith-dropdown").on('mouseenter mouseleave', function () {
        $(this).find(".dropdown").stop().slideToggle();
    });

    console.log("width", $(window).width());

    if ($(window).width() < 768) {
        $(".menu.workwith").click(function (e) {
            e.preventDefault();
            console.log("Prevented Default");
        });

        $(".link-all").click();
    }

});