﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace VBortsvadze.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");



            routes.MapRoute(
                 "BlogByCategory", // Route name
                 "blog/Category/{id}", // URL with parameters
                 new { controller = "Blog", action = "Category", id = UrlParameter.Optional }, // Parameter defaults
                 new string[] { "VBortsvadze.Web.Controllers" }
            );


            routes.MapRoute(
                "BlogSearch", // Route name
                "blog/Search", // URL with parameters
                new { controller = "Blog", action = "Search", slag = UrlParameter.Optional }, // Parameter defaults
                new string[] { "VBortsvadze.Web.Controllers" }
           );

            routes.MapRoute(
                 "Blog", // Route name
                 "blog/{slag}", // URL with parameters
                 new { controller = "Blog", action = "Index", slag = UrlParameter.Optional }, // Parameter defaults
                 new string[] { "VBortsvadze.Web.Controllers" }
            );


            routes.MapRoute(
                 "Default", // Route name
                 "{controller}/{action}/{id}", // URL with parameters
                 new { controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
                 new string[] { "VBortsvadze.Web.Controllers" }
            );


        }
    }
}
