﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VBortsvadze.DL.Models.Entities
{
    public class Blog
    {
        public int Id { get; set; }
        [Display(Name = "Created Date")]
        public DateTime? CreateDate { get; set; }

        [Display(Name = "Update Date")]
        public DateTime? UpdateDate { get; set; }

        [Required]
        [Display(Name = "Description For SEO Optimisation")]
        public string SeoDescription { get; set; }

        [Required]
        [Display(Name = "Title For SEO Optimisation")]
        public string SeoTitle { get; set; }

        [AllowHtml]
        [Required]
        [Display(Name = "Post TiTle")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Post Content")]
        public string Content { get; set; }

        [Required]
        [Display(Name = "Cover image")]
        public string CoverImage { get; set; }

        [Required]
        [Display(Name = "Small Image")]
        public string SmallImage { get; set; }

        [Display(Name = "Pin to Main Page")]
        public bool Pinned { get; set; }

        [Required]
        [Display(Name = "Blog Url On Page")]
        public string Slag { get; set; }

        public List<BlogToCategory> Categories { get; set; }
    }
}