﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using System.Web.Mvc;
using VBortsvadze.BL.Models;

namespace VBortsvadze.Web.Controllers
{
    public class WorkWithVladimerController : ControllerBase
    {
        // GET: WorkWithVladimer
       

       
        // GET: WorkWithVladimer
        public ActionResult Index()
        {
            ViewBag.Title = "Work With Vladimer";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("WorkWith");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();

            return View();
        }

        public ActionResult Consulting()
        {
            ViewBag.Title = "Consulting";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("Consult");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();

            return View();
           
        }
        public ActionResult Speaking()
        {
            ViewBag.Title = "Speaking";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("Speaking");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }


        public ActionResult StartupAdvising()
        {
            ViewBag.Title = "Startup Advising";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("startup");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }


        public ActionResult Teaching()
        {
            ViewBag.Title = "Teaching";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("teach");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }
        public ActionResult Training()
        {
            ViewBag.Title = "Training";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("trainin");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }
        public ActionResult Workshops()
        {
            ViewBag.Title = "Workshops";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("worksh");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }
        public ActionResult Coaching()
        {
            ViewBag.Title = "Coaching";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("coatch");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }
        public ActionResult Mentoring()
        {
            ViewBag.Title = "Mentoring";
            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.testimonials = Service.Testimonials.getByPage("mentor");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            return View();
        }
    }
}