﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Models
{
    public class CustomBlogViewModel
    {
        public Blog Blog { get; set; }
        public List<BlogCategory> Categories { get; set; }
    }
}