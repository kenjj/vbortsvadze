﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Utils;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    public class RecognitionsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/Recognitions
        public ActionResult Index()
        {
            return View(db.Recognitions.ToList());
        }

        // GET: Admin/Recognitions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recognition recognition = db.Recognitions.Find(id);
            if (recognition == null)
            {
                return HttpNotFound();
            }
            return View(recognition);
        }

        // GET: Admin/Recognitions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Recognitions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,Logo,Title,Content,OrderIndex")] Recognition recognition)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    string imageName = recognition.Title.Replace(" ", "-") + Guid.NewGuid() + ".png";
                    var Image = recognition.Logo.Base64ToImage();
                    if (Image == null)
                    {
                        ModelState.AddModelError("Logo", "Invalid Logo");
                        throw new Exception("Invalid Logo");
                    }

                    bool ImageSaved = Image.SaveImage(CommonValues.RecognitionsImages, imageName);
                    if (!ImageSaved)
                    {
                        ModelState.AddModelError("Image", "Cannot Save / Invalid Logo");
                        throw new Exception("Cannot Save / Invalid Logo");
                    }

                    Image.Dispose();


                    recognition.Logo = imageName;


                    db.Recognitions.Add(recognition);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    return View(recognition);
                }
          
            }

            return View(recognition);
        }

        // GET: Admin/Recognitions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recognition recognition = db.Recognitions.Find(id);
            if (recognition == null)
            {
                return HttpNotFound();
            }
            return View(recognition);
        }

        // POST: Admin/Recognitions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,Logo,Title,Content,OrderIndex")] Recognition recognition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recognition).State = EntityState.Modified;


                try
                {

                    string imageName = recognition.Title.Replace(" ", "-") + Guid.NewGuid() + ".png";

                    var Image = recognition.Logo.Base64ToImage();
                    if (Image != null)
                    {
                        bool avatarSaved = Image.SaveImage(CommonValues.RecognitionsImages, imageName);
                        if (avatarSaved)
                        {
                            recognition.Logo= imageName;
                            Image.Dispose();
                        }
                    }

                }
                catch (Exception)
                {
                    return View(recognition);
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(recognition);
        }

        // GET: Admin/Recognitions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recognition recognition = db.Recognitions.Find(id);
            if (recognition == null)
            {
                return HttpNotFound();
            }
            return View(recognition);
        }

        // POST: Admin/Recognitions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Recognition recognition = db.Recognitions.Find(id);
            db.Recognitions.Remove(recognition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
