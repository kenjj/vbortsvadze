﻿
$(document).ready(function () {

    CKEDITOR.replace('Content', {
        filebrowserImageBrowseUrl: Path + 'Blogs/uploadPartial',
        filebrowserImageUploadUrl: Path + 'Blogs/uploadnow'
    });

    new createCropper("CoverImage", 1200, 500);
    new createCropper("SmallImage", 500, 300);

});

function updateValue(id, value) {
    // this gets called from the popup window and updates the field with a new value 
    document.getElementById(id).value = value;
}

