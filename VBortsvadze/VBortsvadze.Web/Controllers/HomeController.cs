﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;
using System.Web.Mvc;
using System.Text.RegularExpressions;
using VBortsvadze.BL.Models;

namespace VBortsvadze.Web.Controllers
{
    public class HomeController : ControllerBase
    {

        public ActionResult Index()
        {
            ViewBag.testimonials = Service.Testimonials.getByPage("home");
            ViewBag.NetworkActivity = Service.Statistic.getNetworkActivity();
            ViewBag.Statistic = Service.Statistic.getHomeStatistic();
            ViewBag.Recognitions = Service.Recognition.getOne();

            var model = Service.Blogs.GetBlogsAndCategories(3);

            ProfileViewModel ProFileModel = new ProfileViewModel()
            {
                InfluenceImage = Service.Statistic.getInfluenceImage(),
                Info = Service.Statistic.getInfos(),
                KloutNumber = Service.Statistic.getCloutNumber(),
                TweeterImage = Service.Statistic.getTweeterImage()
            };

            ViewBag.ProFileModel = ProFileModel;
            ViewBag.Title = "Main";

            return View(model);
        }

        [HttpPost]
        public JsonResult AddSubscriber(string email)
        {
            try
            {
              
                Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                Match match = regex.Match(email);
                if (!match.Success)
                    throw new Exception("Invalid Email");
                else
                {
                    Service.Contact.addEmail(email);
                }
                return Json(new
                {
                    Error = false
                });
            }
            catch (Exception ex)
            {
                return Json(new {
                    Error = ex.Message
                });
            }
        }

        public ActionResult Demo()
        {
            return View();
        }


    }
}