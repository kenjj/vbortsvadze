﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.BL.Utils
{
    public class CommonValues
    {
        public static string AdminSessionKey
        {
            get
            {
                return "AdminLoggedIn";
            }
        }
        public static int MaxImageSize
        {
            get
            {
                return 2 * 1024 * 1024;
            }
        }
       
        
        public static string TestimonialImages
        {
            get
            {
                return "~/Uploads/Images/Testimonials";
            }
        }

        public static string PublicationsImages
        {
            get
            {
                return "~/Uploads/Images/Publications";
            }
        }
        public static string RecognitionsImages
        {
            get
            {
                return "~/Uploads/Images/Recognitions";
            }
        }

        public static string BlogImages
        {
            get
            {
                return "~/Uploads/Images/Blog";
            }
        }

    }
}