﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class KloutNumbersController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/KloutNumbers
        public ActionResult Index()
        {
            var item = db.KloutNumber.FirstOrDefault();
            if (item == null)
            {
                return RedirectToAction("create");
            }else
            {
                return RedirectToAction("edit", new { id = item.Id });
            }
            
        }

        // GET: Admin/KloutNumbers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KloutNumber kloutNumber = db.KloutNumber.Find(id);
            if (kloutNumber == null)
            {
                return HttpNotFound();
            }
            return View(kloutNumber);
        }

        // GET: Admin/KloutNumbers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/KloutNumbers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Number")] KloutNumber kloutNumber)
        {
            if (ModelState.IsValid)
            {
                db.KloutNumber.Add(kloutNumber);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kloutNumber);
        }

        // GET: Admin/KloutNumbers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KloutNumber kloutNumber = db.KloutNumber.Find(id);
            if (kloutNumber == null)
            {
                return HttpNotFound();
            }
            return View(kloutNumber);
        }

        // POST: Admin/KloutNumbers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Number")] KloutNumber kloutNumber)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kloutNumber).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kloutNumber);
        }

        // GET: Admin/KloutNumbers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KloutNumber kloutNumber = db.KloutNumber.Find(id);
            if (kloutNumber == null)
            {
                return HttpNotFound();
            }
            return View(kloutNumber);
        }

        // POST: Admin/KloutNumbers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KloutNumber kloutNumber = db.KloutNumber.Find(id);
            db.KloutNumber.Remove(kloutNumber);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
