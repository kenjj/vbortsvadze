﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class VInfo
    {
        [Key]
        public int Id { get; set; }

        public string InfoName { get; set; }
        public string InfoValue { get; set; }
    }
}