﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Utils;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class AwardsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/Awards
        public ActionResult Index()
        {
            return View(db.Awards.ToList());
        }

        // GET: Admin/Awards/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Award award = db.Awards.Find(id);
            if (award == null)
            {
                return HttpNotFound();
            }
            return View(award);
        }

        // GET: Admin/Awards/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Awards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Date,Logo,Title,Content,OrderIndex")] Award award)
        {
       

            if (ModelState.IsValid)
            {
                try
                {

                    string imageName = award.Title.Replace(" ", "-") + Guid.NewGuid() + ".png";
                    var Image = award.Logo.Base64ToImage();
                    if (Image == null)
                    {
                        ModelState.AddModelError("Logo", "Invalid Logo");
                        throw new Exception("Invalid Logo");
                    }

                    bool ImageSaved = Image.SaveImage(CommonValues.RecognitionsImages, imageName);
                    if (!ImageSaved)
                    {
                        ModelState.AddModelError("Image", "Cannot Save / Invalid Logo");
                        throw new Exception("Cannot Save / Invalid Logo");
                    }

                    Image.Dispose();


                    award.Logo = imageName;


                    db.Awards.Add(award);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                }
                catch (Exception ex)
                {
                    return View(award);
                }

            }

            return View(award);
        }

        // GET: Admin/Awards/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Award award = db.Awards.Find(id);
            if (award == null)
            {
                return HttpNotFound();
            }
            return View(award);
        }

        // POST: Admin/Awards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Date,Logo,Title,Content,OrderIndex")] Award award)
        {
            if (ModelState.IsValid)
            {
                db.Entry(award).State = EntityState.Modified;


                try
                {

                    string imageName = award.Title.Replace(" ", "-") + Guid.NewGuid() + ".png";

                    var Image = award.Logo.Base64ToImage();
                    if (Image != null)
                    {
                        bool avatarSaved = Image.SaveImage(CommonValues.RecognitionsImages, imageName);
                        if (avatarSaved)
                        {
                            award.Logo = imageName;
                            Image.Dispose();
                        }
                    }

                }
                catch (Exception)
                {
                    return View(award);
                }

                db.SaveChanges();
                return RedirectToAction("Index");
                
            }
            return View(award);
        }

        // GET: Admin/Awards/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Award award = db.Awards.Find(id);
            if (award == null)
            {
                return HttpNotFound();
            }
            return View(award);
        }

        // POST: Admin/Awards/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Award award = db.Awards.Find(id);
            db.Awards.Remove(award);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
