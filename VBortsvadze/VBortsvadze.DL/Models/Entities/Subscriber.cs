﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class Subscriber
    {
        public int Id { get; set; }
        public string Email { get; set; }
    }
}