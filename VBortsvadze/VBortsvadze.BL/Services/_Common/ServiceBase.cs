﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models;

namespace VBortsvadze.BL.Services._Common
{
    public class ServiceBase
    {
        protected EFDbContext Db { get; set; }
        public ServiceBase(EFDbContext db)
        {
            Db = db;
        }
    }
}