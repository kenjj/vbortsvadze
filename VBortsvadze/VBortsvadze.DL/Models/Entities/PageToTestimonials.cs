﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class PageToTestimonials
    {
        public int Id { get; set; }

        [ForeignKey("Testimonial")]
        public int TestimonialId { get; set; }
        public Testimonial Testimonial { get; set; }

        [ForeignKey("Page")]
        [Required]
        [MaxLength(8)]
        public string PageName { get; set; }
        public Page Page { get; set; }
    }
}