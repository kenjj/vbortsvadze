﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class PagesController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/Pages
        public ActionResult Index()
        {
            return View(db.Pages.ToList());
        }

        // GET: Admin/Pages/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // GET: Admin/Pages/Create
        public ActionResult Create()
        {
            ViewBag.testimonials = db.Testimonials.ToList();
            return View();
        }

        // POST: Admin/Pages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Page page, List<int> testimonialIds)
        {
            ViewBag.testimonials = db.Testimonials.ToList();
            if (ModelState.IsValid)
            {
                db.Pages.Add(page);
                db.SaveChanges();


                foreach (var item in testimonialIds)
                {
                    db.PageToTestimonials.Add(new PageToTestimonials()
                    {
                        PageName = page.Name,
                        TestimonialId = item
                    });
                }
                db.SaveChanges();


                return RedirectToAction("Index");
            }

            return View(page);
        }

        // GET: Admin/Pages/Edit/5
        public ActionResult Edit(string id)
        {
            ViewBag.testimonials = db.Testimonials.ToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Include(Queryable=>Queryable.Testimonials).FirstOrDefault(q=>q.Name==id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: Admin/Pages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Name")] Page page, List<int> testimonialIds)
        {
            ViewBag.testimonials = db.Testimonials.ToList();
            try
            {
              
                if (ModelState.IsValid)
                {
                    db.Entry(page).State = EntityState.Modified;
                    db.SaveChanges();

                    var pgCategories = db.PageToTestimonials
                       .Where(q => q.PageName == page.Name && !testimonialIds.Contains(q.TestimonialId));

                    db.PageToTestimonials.RemoveRange(pgCategories);

                    var pageCategories = db.PageToTestimonials
                        .Where(q => q.PageName == page.Name);

                    foreach (var item in testimonialIds)
                    {
                        if (pageCategories.FirstOrDefault(q => q.TestimonialId == item) == null)
                        {
                            db.PageToTestimonials.Add(new PageToTestimonials()
                            {
                                PageName = page.Name,
                                TestimonialId = item
                            });
                        }
                    }

                    db.SaveChanges();


                    return RedirectToAction("Index");
                }
            }
            catch (DbEntityValidationException e)
            {

                foreach (var eve in e.EntityValidationErrors)
                {
                   
                    foreach (var ve in eve.ValidationErrors)
                    {
                       
                    }
                }
            }
           
            return View(page);
        }

        // GET: Admin/Pages/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Page page = db.Pages.Find(id);
            if (page == null)
            {
                return HttpNotFound();
            }
            return View(page);
        }

        // POST: Admin/Pages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Page page = db.Pages.Find(id);
            db.Pages.Remove(page);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
