﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class VInfoesController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/VInfoes
        public ActionResult Index()
        {
            return View(db.VInfos.ToList());
        }

        // GET: Admin/VInfoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VInfo vInfo = db.VInfos.Find(id);
            if (vInfo == null)
            {
                return HttpNotFound();
            }
            return View(vInfo);
        }

        // GET: Admin/VInfoes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/VInfoes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,InfoName,InfoValue")] VInfo vInfo)
        {
            if (ModelState.IsValid)
            {
                db.VInfos.Add(vInfo);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(vInfo);
        }

        // GET: Admin/VInfoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VInfo vInfo = db.VInfos.Find(id);
            if (vInfo == null)
            {
                return HttpNotFound();
            }
            return View(vInfo);
        }

        // POST: Admin/VInfoes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,InfoName,InfoValue")] VInfo vInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vInfo);
        }

        // GET: Admin/VInfoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            VInfo vInfo = db.VInfos.Find(id);
            if (vInfo == null)
            {
                return HttpNotFound();
            }
            return View(vInfo);
        }

        // POST: Admin/VInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            VInfo vInfo = db.VInfos.Find(id);
            db.VInfos.Remove(vInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
