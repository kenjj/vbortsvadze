﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Utils;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.DL.ViewModels.Admin.Blog;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class BlogsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/Blogs
        public ActionResult Index()
        {
            
            return View(db.Blogs.ToList());
        }
       
        public async Task SendToSubscribers(Blog blog)
        {
            var subscribers = db.Subscribers.ToList();
            foreach (var item in subscribers)
            {
                var message =
                "<div>" +
                    "<h3>Vladimer Has Just added new blog</h3>" +
                    "<h4><a href='https://vladimerbotsvadze.com/blog/"+blog.Slag+"'> Blog - " + blog.Title + "</a></h4>" +
                "</div>";

                try
                {
                    EmaliHelper.SendEmailByAddress(item.Email,message);
                }
                catch
                {
                   
                }
            }
          

        }

        // GET: Admin/Blogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // GET: Admin/Blogs/Create
        public ActionResult Create()
        {
            ViewBag.Categories = db.BlogCategories.OrderBy(q=>q.OrderIndex).ToList();
            return View();
        }

        // POST: Admin/Blogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public async Task<ActionResult> Create([Bind(Include = "Id,CreateDate,UpdateDate,SeoDescription,SeoTitle,Title,Content,CoverImage,SmallImage,Pinned,Slag")] Blog blog, List<int> categoryIds)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    var coverImg = CommonUtils.Base64ToImage(blog.CoverImage);
                    if (coverImg == null)
                    {
                        ModelState.AddModelError("CoverImage", "Invalid Image");
                        throw new Exception("Invalid Cover Image");
                    }

                    var smallImg = CommonUtils.Base64ToImage(blog.SmallImage);
                    if (smallImg == null)
                    {
                        ModelState.AddModelError("SmallImage", "Invalid Image");
                        throw new Exception("Invalid Small Image");
                    }

                    string fileNames = blog.Title.Replace(" ", "-");

                    string coverImgName = fileNames + "-cover.png";
                    string smallImgName = fileNames + "-small.png";



                    bool coverImageSaved = coverImg.SaveImage(CommonValues.BlogImages, coverImgName);
                    if (!coverImageSaved)
                    {
                        ModelState.AddModelError("CoverImage", "Cannot Save / Invalid Image");
                        throw new Exception("Cannot Save / Invalid Cover Image");
                    }


                    bool smallImageSaved = smallImg.SaveImage(CommonValues.BlogImages, smallImgName);
                    if (!smallImageSaved)
                    {
                        ModelState.AddModelError("SmallImage", "Cannot Save / Invalid Image");
                        throw new Exception("Cannot Save / Invalid Small Image");
                    }

                    coverImg.Dispose();
                    smallImg.Dispose();


                    blog.CoverImage = coverImgName;
                    blog.SmallImage = smallImgName;
                    blog.CreateDate = blog.UpdateDate = DateTime.Now;

                    if (string.IsNullOrEmpty(blog.Slag))
                    {
                        blog.Slag = blog.Title;
                    }

                    blog.Slag = blog.Slag.Replace(" ", "-");
                    blog.Slag = Server.UrlEncode(blog.Slag);

                    db.Blogs.Add(blog);
                    db.SaveChanges();



                    foreach (var item in categoryIds)
                    {
                        db.BlogToCategorys.Add(new BlogToCategory()
                        {
                            BlogId = blog.Id,
                            CategoryId = item
                        });
                    }

                    db.SaveChanges();


                    ViewBag.Categories = db.BlogCategories.OrderBy(q => q.OrderIndex).ToList();

                    await SendToSubscribers(blog);

                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    ViewBag.Categories = db.BlogCategories.OrderBy(q => q.OrderIndex).ToList();
                    return View(blog);
                }
            }
            else
            {

                var errors = ModelState.Select(x => x.Value.Errors)
                         .Where(y => y.Count > 0)
                         .ToList();
                var p = errors;
            }

            ViewBag.Categories = db.BlogCategories.OrderBy(q => q.OrderIndex).ToList();
            return View(blog);
        }

        // GET: Admin/Blogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Include(s => s.Categories).FirstOrDefault(s => s.Id == id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            ViewBag.Categories = db.BlogCategories.OrderBy(q => q.OrderIndex).ToList();
            return View(blog);
        }

        // POST: Admin/Blogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Edit([Bind(Include = "Id,CreateDate,UpdateDate,SeoDescription,SeoTitle,Title,Content,CoverImage,SmallImage,Pinned,Slag")] Blog blog, List<int> categoryIds)
        {
            if (ModelState.IsValid)
            {
                
                db.Entry(blog).State = EntityState.Modified;
                blog.UpdateDate = DateTime.Now;

                var blCategories = db.BlogToCategorys
                    .Where(q => q.BlogId == blog.Id && !categoryIds.Contains(q.CategoryId));

                db.BlogToCategorys.RemoveRange(blCategories);

                var blogCategories = db.BlogToCategorys
                    .Where(q => q.BlogId == blog.Id);

                foreach (var item in categoryIds)
                {
                    if (blogCategories.FirstOrDefault(q => q.CategoryId == item) == null)
                    {
                        db.BlogToCategorys.Add(new BlogToCategory()
                        {
                            BlogId = blog.Id,
                            CategoryId = item
                        });
                    }
                }

                db.SaveChanges();


                return RedirectToAction("Index");
            }


            return View(blog);
        }

        // GET: Admin/Blogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Blog blog = db.Blogs.Find(id);
            if (blog == null)
            {
                return HttpNotFound();
            }
            return View(blog);
        }

        // POST: Admin/Blogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Blog blog = db.Blogs.Find(id);
            db.Blogs.Remove(blog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        public void Uploadnow(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/Uploads/Images/Blog"), ImageName);
                upload.SaveAs(path);
            }

        }

        public ActionResult UploadPartial()
        {
            var appData = Server.MapPath("~/Uploads/Images/Blog");
            //var images = Directory.GetFiles(appData).Select(x => new ImagesViewModel
            //{
            //    Url =  Url.Content("~/Uploads/Images/Blog/" + Path.GetFileName(x))
            //});

            DirectoryInfo info = new DirectoryInfo(appData);
            var images = info.GetFiles().OrderByDescending(p => p.CreationTime).Take(14).Select(x => new ImagesViewModel
            {
                Url = Url.Content("~/Uploads/Images/Blog/" + Path.GetFileName(x.FullName))
            });

            return View(images);
        }








        protected override void Dispose(bool disposing)
       { 
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
