﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class SocialNetworkActivitiesController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/SocialNetworkActivities
        public ActionResult Index()
        {
            return View(db.SocialNetworkActivities.ToList());
        }

        // GET: Admin/SocialNetworkActivities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SocialNetworkActivity socialNetworkActivity = db.SocialNetworkActivities.Find(id);
            if (socialNetworkActivity == null)
            {
                return HttpNotFound();
            }
            return View(socialNetworkActivity);
        }

        // GET: Admin/SocialNetworkActivities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/SocialNetworkActivities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,ActivityCount")] SocialNetworkActivity socialNetworkActivity)
        {
            if (ModelState.IsValid)
            {
                db.SocialNetworkActivities.Add(socialNetworkActivity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(socialNetworkActivity);
        }

        // GET: Admin/SocialNetworkActivities/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SocialNetworkActivity socialNetworkActivity = db.SocialNetworkActivities.Find(id);
            if (socialNetworkActivity == null)
            {
                return HttpNotFound();
            }
            return View(socialNetworkActivity);
        }

        // POST: Admin/SocialNetworkActivities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,ActivityCount")] SocialNetworkActivity socialNetworkActivity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(socialNetworkActivity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(socialNetworkActivity);
        }

        // GET: Admin/SocialNetworkActivities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SocialNetworkActivity socialNetworkActivity = db.SocialNetworkActivities.Find(id);
            if (socialNetworkActivity == null)
            {
                return HttpNotFound();
            }
            return View(socialNetworkActivity);
        }

        // POST: Admin/SocialNetworkActivities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SocialNetworkActivity socialNetworkActivity = db.SocialNetworkActivities.Find(id);
            db.SocialNetworkActivities.Remove(socialNetworkActivity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
