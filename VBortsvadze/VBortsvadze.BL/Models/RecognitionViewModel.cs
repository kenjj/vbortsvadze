﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Models
{
    public class RecognitionViewModel
    {
        public int Year { get; set; }
        public List<Recognition> List { get; set; }
    }
}