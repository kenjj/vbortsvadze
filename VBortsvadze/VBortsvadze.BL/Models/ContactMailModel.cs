﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.BL.Models
{
    public class ContactMailModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}