﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.BL.Utils;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class InfluenceImagesController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/InfluenceImages
        public ActionResult Index()
        {
            var item = db.InfluenceImage.FirstOrDefault();
            if (item == null)
            {
                return RedirectToAction("create");
            }
            else
            {
                return RedirectToAction("edit", new { id = item.Id });
            }
         
        }

        // GET: Admin/InfluenceImages/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfluenceImage influenceImage = db.InfluenceImage.Find(id);
            if (influenceImage == null)
            {
                return HttpNotFound();
            }
            return View(influenceImage);
        }

        // GET: Admin/InfluenceImages/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/InfluenceImages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Image")] InfluenceImage influenceImage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var name = Guid.NewGuid().ToString() + ".png";

                    var img = influenceImage.Image.Base64ToImage();
                    if (img == null)
                    {
                        ModelState.AddModelError("Image", "Invalid Image");
                        throw new Exception("Invalid Image");
                    }

                    bool ImageSaved = img.SaveImage(CommonValues.TestimonialImages, name);
                    if (!ImageSaved)
                    {
                        ModelState.AddModelError("Image", "Cannot Save / Invalid Image");
                        throw new Exception("Cannot Save / Invalid Image");
                    }

                    img.Dispose();
                    influenceImage.Image = name;

                    db.InfluenceImage.Add(influenceImage);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    return View(influenceImage);
                }
                
            }

            return View(influenceImage);
        }

        // GET: Admin/InfluenceImages/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfluenceImage influenceImage = db.InfluenceImage.Find(id);
            if (influenceImage == null)
            {
                return HttpNotFound();
            }
            return View(influenceImage);
        }

        // POST: Admin/InfluenceImages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Image")] InfluenceImage influenceImage)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(influenceImage).State = EntityState.Modified;

                    var name = Guid.NewGuid().ToString() + ".png";

                    var img = influenceImage.Image.Base64ToImage();
                    if (img == null)
                    {
                        ModelState.AddModelError("Image", "Invalid Image");
                        throw new Exception("Invalid Image");
                    }

                    bool ImageSaved = img.SaveImage(CommonValues.TestimonialImages, name);
                    if (!ImageSaved)
                    {
                        ModelState.AddModelError("Image", "Cannot Save / Invalid Image");
                        throw new Exception("Cannot Save / Invalid Image");
                    }

                    img.Dispose();
                    influenceImage.Image = name;

                   
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    return View(influenceImage);
                }

            
           
               
            }
            return View(influenceImage);
        }

        // GET: Admin/InfluenceImages/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InfluenceImage influenceImage = db.InfluenceImage.Find(id);
            if (influenceImage == null)
            {
                return HttpNotFound();
            }
            return View(influenceImage);
        }

        // POST: Admin/InfluenceImages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InfluenceImage influenceImage = db.InfluenceImage.Find(id);
            db.InfluenceImage.Remove(influenceImage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
