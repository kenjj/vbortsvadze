﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class Publication
    {
        public int Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
        public string SourceUrl { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}