﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VBortsvadze.DL.Models.Entities;

namespace VBortsvadze.BL.Models
{
    public class BlogViewModel
    {
        public List<Blog> Blogs { get; set; }
        public List<BlogCategory> Categories { get; set; }
        public int CurrentCategoryId { get; set; }
    }
}