﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControllerBase = VBortsvadze.Web.Controllers._Common.ControllerBase;

namespace VBortsvadze.Web.Controllers
{
    public class AwardsController : ControllerBase
    {
        // GET: Awards
        public ActionResult Index()
        {
            var model = Service.Recognition.getAll();
            ViewBag.Title = "Awards";
            return View(model);
        }
    }
}