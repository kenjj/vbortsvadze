﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.BL.Utils;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class PublicationsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Admin/Publications
        public ActionResult Index()
        {
            return View(db.Publications.ToList());
        }

        // GET: Admin/Publications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Publication publication = db.Publications.Find(id);
            if (publication == null)
            {
                return HttpNotFound();
            }
            return View(publication);
        }

        // GET: Admin/Publications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Publications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Image,Title,SourceUrl,CreateDate,UpdateDate")] Publication publication)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string imageName = publication.Title.Replace(" ", "-") + Guid.NewGuid()  + ".png";

                    publication.CreateDate = DateTime.Now;
                    publication.UpdateDate = DateTime.Now;



                    var Image = publication.Image.Base64ToImage();
                    if (Image == null)
                    {
                        ModelState.AddModelError("Image", "Invalid Image");
                        throw new Exception("Invalid Image");
                    }

                    bool ImageSaved = Image.SaveImage(CommonValues.PublicationsImages, imageName);
                    if (!ImageSaved)
                    {
                        ModelState.AddModelError("Image", "Cannot Save / Invalid Image");
                        throw new Exception("Cannot Save / Invalid Image");
                    }

                    Image.Dispose();


                    publication.Image = imageName;

                    db.Publications.Add(publication);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    return View(publication);
                }
               
            }

            return View(publication);
        }

        // GET: Admin/Publications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Publication publication = db.Publications.Find(id);
            if (publication == null)
            {
                return HttpNotFound();
            }
            return View(publication);
        }

        // POST: Admin/Publications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Image,Title,SourceUrl,CreateDate,UpdateDate")] Publication publication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(publication).State = EntityState.Modified;


                try
                {

                    string imageName = publication.Title.Replace(" ", "-") + Guid.NewGuid() + ".png";

                    var Image = publication.Image.Base64ToImage();
                    if (Image != null)
                    {
                        bool avatarSaved = Image.SaveImage(CommonValues.PublicationsImages, imageName);
                        if (avatarSaved)
                        {
                            publication.Image = imageName;
                            Image.Dispose();
                        }


                    }

                }
                catch (Exception)
                {
                    return View(publication);
                }

                publication.UpdateDate = DateTime.Now;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(publication);
        }

        // GET: Admin/Publications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Publication publication = db.Publications.Find(id);
            if (publication == null)
            {
                return HttpNotFound();
            }
            return View(publication);
        }

        // POST: Admin/Publications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Publication publication = db.Publications.Find(id);
            db.Publications.Remove(publication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
