﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class Testimonial
    {
        public int Id { get; set; }

        [Required]
        public string Avatar { get; set; }

      
        [Display(Name = "Company Logo")]
        public string CompanyLogo { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Required]
        [Display(Name = "Testimonial Position At Company")]
        public string CompanyPosition { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required]
        [Display(Name = "She/He Recomends...")]
        public string RecomendationPrase { get; set; }

        [Required]
        [Display(Name = "Order Index")]
        public int OrderIndex { get; set; }
    }
}