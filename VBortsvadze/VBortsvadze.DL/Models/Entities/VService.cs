﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VBortsvadze.DL.Models.Entities
{
    public class VService
    {
        public int Id { get; set; }
        public string SeoDescription { get; set; }
        public string SeoTitle { get; set; }
        public string MainTitle { get; set; }
        public string PageName { get; set; }
        public string Content { get; set; }
        public int OrderIndex { get; set; }
        public List<VServiceToTestimonial> Testimonials { get; set; }
    }
}