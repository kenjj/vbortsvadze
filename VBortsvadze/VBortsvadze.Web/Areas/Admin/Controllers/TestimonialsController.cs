﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VBortsvadze.DL.Models;
using VBortsvadze.DL.Models.Entities;
using VBortsvadze.BL.Utils;
using VBortsvadze.Web.Areas.Admin.Filters;

namespace VBortsvadze.Web.Areas.Admin.Controllers
{
    [Auth]
    public class TestimonialsController : Controller
    {
        private EFDbContext db = new EFDbContext();

        // GET: Testimonials
        public ActionResult Index()
        {
            return View(db.Testimonials.ToList());
        }

        // GET: Testimonials/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testimonial testimonial = db.Testimonials.Find(id);
            if (testimonial == null)
            {
                return HttpNotFound();
            }
            return View(testimonial);
        }

        // GET: Testimonials/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Testimonials/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Avatar,CompanyLogo,FullName,CompanyPosition,CompanyName,RecomendationPrase,OrderIndex")] Testimonial testimonial)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string fileNames = testimonial.FullName.Replace(" ", "-");

                    string avatarFileName = fileNames + "-avatar.png";
                    string companyLogoFileName = fileNames + "-company-logo.png";

                    #region Validations And Convert Image To base64

                        var avatarImage = testimonial.Avatar.Base64ToImage();
                        if (avatarImage == null)
                        {
                            ModelState.AddModelError("Avatar", "Invalid Image");
                            throw new Exception("Invalid Avatar Image");
                        }




                    #endregion
                    #region Save Image Files
                    bool avatarSaved = avatarImage.SaveImage(CommonValues.TestimonialImages, avatarFileName);
                    if (!avatarSaved)
                    {
                        ModelState.AddModelError("Avatar", "Cannot Save / Invalid Image");
                        throw new Exception("Cannot Save / Invalid Avatar Image");
                    }

                    avatarImage.Dispose();

#endregion

                    var companyLogo = testimonial.CompanyLogo.Base64ToImage();
                    if (companyLogo != null)
                    {
                        bool companyLogoSaved = companyLogo.SaveImage(CommonValues.TestimonialImages, companyLogoFileName);
                        if (companyLogoSaved)
                        {
                            testimonial.CompanyLogo = companyLogoFileName;
                        }else
                        {
                            testimonial.CompanyLogo = string.Empty;
                        }
                        companyLogo.Dispose();
                    }else
                    {
                        testimonial.CompanyLogo = string.Empty;
                    }
                   
                    

                

                    




                 

                     testimonial.Avatar = avatarFileName;
                    
                     db.Testimonials.Add(testimonial);
                     db.SaveChanges();
                     return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    return View(testimonial);
                }
            }

            return View(testimonial);
        }

        // GET: Testimonials/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testimonial testimonial = db.Testimonials.Find(id);
            if (testimonial == null)
            {
                return HttpNotFound();
            }
            return View(testimonial);
        }

        // POST: Testimonials/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Avatar,CompanyLogo,FullName,CompanyPosition,CompanyName,RecomendationPrase,OrderIndex")] Testimonial testimonial)
        {
            if (ModelState.IsValid)
            {
                db.Entry(testimonial).State = EntityState.Modified;

                try
                {
                    string fileNames = testimonial.FullName.Replace(" ", "-");
                    string avatarFileName = fileNames + "-avatar.png";
                    string companyLogoFileName = fileNames + "-company-logo.png";



                    #region If passed image Then Save Them

                        var avatarImage = testimonial.Avatar.Base64ToImage();
                        if (avatarImage != null)
                        {
                            bool avatarSaved = avatarImage.SaveImage(CommonValues.TestimonialImages, avatarFileName);
                            if (avatarSaved)
                            {
                                testimonial.Avatar = avatarFileName;
                                avatarImage.Dispose();
                            }
                       
                        }

                        var companyLogo = testimonial.CompanyLogo.Base64ToImage();
                        if (companyLogo != null)
                        {
                            bool companyLogoSaved = companyLogo.SaveImage(CommonValues.TestimonialImages, companyLogoFileName);
                            if (companyLogoSaved)
                            {
                                testimonial.CompanyLogo = companyLogoFileName;
                                companyLogo.Dispose();
                            }     
                        }

                    #endregion


                }
                catch (Exception)
                {
                    return View(testimonial);
                }


                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(testimonial);
        }

        // GET: Testimonials/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Testimonial testimonial = db.Testimonials.Find(id);
            if (testimonial == null)
            {
                return HttpNotFound();
            }
            return View(testimonial);
        }

        // POST: Testimonials/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Testimonial testimonial = db.Testimonials.Find(id);
            db.Testimonials.Remove(testimonial);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
